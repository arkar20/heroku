var mongoose=require('mongoose')
require('dotenv').config();

var url=process.env.MONGO_URL
console.log(process.env.MONGO_URL);

mongoose.connect(url,{
    useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true 
}).then(()=>console.log('connected'))

const noteSchema=new mongoose.Schema({
  name:{type: String,    minlength: 5,    required: true},
  number:String,
  created_at:Date
})



noteSchema.set('toJSON',{
    transform:(docement,returnObject)=>{
        returnObject.id=returnObject._id.toString()
        delete  returnObject._id
        delete  returnObject.__v
    }
})
module.exports=mongoose.model('persons',noteSchema);