var express=require('express')
var app=express()
var cors=require('cors')
app.use(express.static('build'))
app.use(express.json())
app.use(cors())

var Note=require('./models/note');

require('dotenv').config();
const unknownid=(req,res)=>{
  res.status(404).json("Unknown ID")
}

app.get('/api/persons',(req,res)=>{
   Note.find({}).then(data=>{
    return res.json(data)
  })
})

app.get('/api/persons/:id',(req,res)=>{
  console.log(req.params.id)
    Note.findById(req.params.id).then(data=>{
      return res.json(data)
    }).catch(err=>res.status(404).json("ID not Found"))
})

app.delete('/api/persons/:id',(req,res)=>{
    // const id=Number(req.params.id)
  Note.findOneAndDelete(req.params.id).then(()=>{
    console.log('successful delete')
    return res.json("successful")
  })
})


app.post('/api/persons',(req,res)=>{
    // const id=Math.floor(Math.random()*100000)
if(req.body==undefined)
        return res.json("content Empty")
    const newData=new Note({
       
        name:req.body.name,
        number:req.body.number
    })
   newData.save()
   .then(formatdata=>{return formatdata.toJSON()})
   .then(data=>{ 
              console.log(data)
              res.json(data)})
    .catch(error=>
     res.status(404).json(error)
)
})


app.patch('/api/persons/:id',(req,res)=>{
  // const id=Number(req.params.id)
  const personsnewInfo={
    name:req.body.name,
    number:req.body.number
}
Note.findByIdAndUpdate(req.params.id,personsnewInfo,{new :true}).then(()=>{
  console.log('successful update')
  return res.json(`${req.params.id} is updated`)
}).catch(err=>{
  console.log(err)
  return res.status(404).json("Something Went Wrong")
})
})
const PORT=process.env.PORT||3001
app.listen(PORT,()=>{
    console.log(`server is running ${PORT}`)
})